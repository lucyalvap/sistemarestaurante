﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Local
    {
        public int IdLocal { get; set; }
        public string  Nombre { get; set; }
        public string Distrito { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

    }
}
